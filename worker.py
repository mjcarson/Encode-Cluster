__author__ = 'michael'
import socket, thread, os, time, subprocess, sys

host = "0.0.0.0"
port = 8398
name = "node1"
status = 0

def convert_prog(server):
    global status
    while 1:
        if status == 1:
            server.send("working")
        elif status == 2:
            server.send("done")
            status = 3
            thread.exit()
        time.sleep(10)

def listener( server):
    global status
    while 1:
        # keeps the connection alive
        code = server.recv(1024)
        if code == "??":
            server.send("OK")

        # stops the keep alive and receives the segment that this node will be converting
        elif code == "orders":
            status = 1
            file_size = server.recv(1024)
            file_size = int(file_size)
            ext = server.recv(1024)
            fp = "orig_segment" + ext
            f = open(fp, "wb+")
            print "[*] Receiving file"
            while file_size > 0:
                buffer = server.recv(1024)
                if buffer < 1024:
                    print "null"
                f.write(buffer)
                file_size -= len(buffer)
            f.close()
            print "[*] File received"
            thread.start_new_thread(convert_prog, (server,))

            # converts the file to designated container and codec
            command = "HandBrakeCLI -i " + fp + " -o transcoded_segment.mkv -e x264 --encoder-preset veryslow --encoder-profile high --encoder-level 4.1 -q 20"
            process = subprocess.Popen(command, shell=True)
            process.wait()
            status = 2

            # sends transcoded segment back to server
            while status != 3:
                time.sleep(5)
            print "[*] sending file"
            file_name = "transcoded_segment.mkv"
            file_size = str(os.stat(file_name).st_size)
            server.send(file_size)
            time.sleep(1)
            file_size = int(file_size)
            segment_file = open(file_name, "rb")
            progress_interval = file_size / 50
            progress = file_size - progress_interval
            sys.stdout.write("[%s]" % (" " * 50))
            sys.stdout.flush()
            sys.stdout.write("\b" * (50+1))
            while file_size > 0:
                buffer = segment_file.read(1024)
                server.send(buffer)
                file_size -= len(buffer)
                if file_size < progress:
                    progress -= progress_interval
                    sys.stdout.write("#")
                    sys.stdout.flush()
            sys.stdout.write("\n")
            print "[*] file sent"
            os.remove("orig_segment" + ext)
            os.remove("transcoded_segment.mkv")
            # waits for all nodes to finish
            while server.recv(1024) == "wait":
                time.sleep(10)
            time.sleep(1)
            server.send("OK")


def main():
    server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server.settimeout(180)
    server.connect((host, port))
    if server.recv(1024) == "?name?":
        server.send(name)
        if server.recv(1024) != name:
            print "[!] Name confirmation not received exiting"
            exit(1)
    print "[*] Connected to server[" + host + ":" + str(port) + "]"
    thread.start_new_thread(listener, (server,))
    while 1:
        time.sleep(30)


if __name__ == '__main__':
    main()
