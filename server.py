__author__ = 'michael'
import socket, thread, time, os, subprocess, re, sys
from decimal import Decimal

# globals
# host and port is where the server will listen fro clients
# load_dir is where the server will find files to convert
# targ_dir is where it will save the files after they are converted
# work_dir is where it will store the slices while converting

host = "0.0.0.0"
port = 8398
load_dir = "/mnt/4125C8F34FDB9D66/Media/cluster_tests/load_dir/"
targ_dir = "/mnt/4125C8F34FDB9D66/Media/cluster_tests/targ_dir/"
work_dir = "/mnt/4125C8F34FDB9D66/Media/cluster_tests/work_dir/"

class Roster():
    def __init__(self):
        self.list = []
        self.cnt = -1
        self.connected = 0
        self.nodes = []
        self.systemstatus = 0
        self.segment_status = []
        self.audio_status = 0
class Worker():
    def __init__(self):
        self.addr = "MULL"
        self.name = "NULL"
        self.slice = -1
        self.status = -1
        self.comms = 0
        self.client = "NULL"
        self.num = -1


workers = Roster()


def time_finder(path):
    # Finds the total time of the video in seconds
    # Not my code I found it at http://www.codingwithcody.com/2012/04/get-video-duration-with-ffmpeg-and-python/
    # All credit for this function goes to the guys there
    # Edited a bit to make it return only full seconds and not decimals

    process = subprocess.Popen(['/usr/bin/ffmpeg', '-i', path], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, stderr = process.communicate()
    matches = re.search(r"Duration:\s{1}(?P<hours>\d+?):(?P<minutes>\d+?):(?P<seconds>\d+\.\d+?),", stdout, re.DOTALL).groupdict()

    hours = int(matches['hours'])
    minutes = int(matches['minutes'])
    seconds = Decimal(matches['seconds'])

    total = 0
    total += 60 * 60 * hours
    total += 60 * minutes
    total += seconds
    total = str(total)
    total = total.split(".")
    total = int(total[0])
    return total



def t_handle(client, address, s, workers):
    client.send("?name?")
    nodename =client.recv(1024)
    print "[*]", nodename, "conected", address
    time.sleep(1)
    client.send(nodename)
    workers.cnt += 1
    workers.list.append(Worker())
    workers.list[workers.cnt].name = nodename
    workers.nodes.append(nodename)
    workers.list[workers.cnt].addr = address
    workers.list[workers.cnt].status = 1
    workers.list[workers.cnt].client = client
    workers.list[workers.cnt].socket = s
    workers.list[workers.cnt].num = workers.cnt
    thread.exit()


def listener( workers):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host, port))
    s.listen(1)
    i = -1
    while 1:
        i += 1
        client, address = s.accept()
        thread.start_new_thread(t_handle, (client, address, s, workers))


def rollcall( workers):
    while 1:
        #i = 0
        if workers.systemstatus == 0:
            for i in xrange(0, workers.cnt + 1):
                if workers.list[i].status != -2:
                    workers.list[i].client.send("??")
                    workers.list[i].socket.settimeout(10)
                    code = workers.list[i].client.recv(1024)
                    if code != "OK":
                        workers.list[i].status = -2
                        print "[!] node at", workers.list[i].addr, "is not responding"
                    elif code == "OK":
                        workers.list[i].status = 1
            time.sleep(30)

        # Sends the keep alive to nodes that are still waiting for their segment
        elif workers.systemstatus == 1:
            for worker in workers.list:
                if worker.status != -2:
                    if worker.comms == 0:
                        worker.client.send("??")
            time.sleep(30)


def manager(workers, segment_num, nodes, file_name):
    while 1:
        # waits for transcode to finish then receives it
        if workers.list[nodes[segment_num]].client.recv(1024) == "done":
            file_size = int(workers.list[nodes[segment_num]].client.recv(1024))
            fname = work_dir + file_name
            f = open(file_name, "wb+")
            while file_size > 0:
                data_buffer = workers.list[nodes[segment_num]].client.recv(1024)
                f.write(data_buffer)
                file_size -= len(data_buffer)
            f.close()
            workers.segment_status[segment_num] = "done"

            while workers.systemstatus == 1:
                workers.list[nodes[segment_num]].client.send("wait")
                time.sleep(30)
            thread.exit()

        time.sleep(5)


def main():
    global workers
    thread.start_new_thread(listener, (workers,))
    thread.start_new_thread(rollcall, (workers,))
    print "[*] Waiting for nodes to connect (60 sec)"
    time.sleep(10)
    while 1:
        if os.listdir(load_dir):
            # this will make sure the file is not being still moved to directory
            # it does this by checking the file size and seeing if it changes every 30 seconds
            # fname has extension while fields[0] does not
            # ext contains just the extension of original file
            print "[*] File found\n[*] Making sure file is done being copied"
            file = os.listdir(load_dir)
            fname = file[0]
            fields = fname.split(".")
            ext = "." + fields[1]
            file = load_dir + file[0]
            size = os.stat(file).st_size
            size_check = -1
            while size != size_check:
                print "[*] File size = ", size
                time.sleep(5)
                size_check = size
                size = os.stat(file).st_size

            # finds number of nodes currently connected
            connected = 0
            nodes = []
            for node in workers.list:
                if node.status == 1:
                    connected += 1
                    nodes.append(node.num)

            # checks if workers are connected
            if connected == 0:
                time.sleep(30)
                print "[!] No workers connected - waiting for workers"

            for i in xrange(0, connected):
                workers.list[i].comms = 0

            # finds segment time
            # checks to make sure segments don't become too small to prevent major degradation of quality
            # set segment_time < X to whatever time in seconds you want to be the minimum
            # quota is number of nodes that will be assigned this job
            # quota is reset to number of connected nodes but if segment_time is too low quota is lowered
            total_time = time_finder(file)
            segment_time = total_time / connected
            segment_time += 1
            quota = connected
            print connected, "<<earlierconnected"
            print quota, "<<earlierquota"
            print segment_time, "<<earliersegment_time"
            if segment_time < 60:
                print "noway"
                segment_time = 60
                quota = total_time//segment_time
                if quota == 0:
                    print "get out!!@#"
                    quota = 1
            print segment_time, "<<segment_time"
            print connected, "<<connected"
            print quota, "<<quota"

            # splits the video based on number of workers connected
            command = "ffmpeg -i \"" + file + "\" -acodec copy -f segment -segment_time " + str(segment_time) + " -vcodec copy -reset_timestamps 1 -map 0 -an -sn \"" + work_dir + "segment-%d" + ext + "\""
            process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
            process.wait()

            # sends files to workers
            for i in xrange(0, quota):
                # preps worker for to receive segments
                workers.systemstatus = 1
                workers.list[nodes[i]].comms = 1
                workers.list[nodes[i]].slice = i
                workers.segment_status.append("no")
                workers.list[nodes[i]].client.send("orders")

                # prepares to send segment
                file_name = work_dir + "segment-" + str(i) + ext
                file_size = str(os.stat(file_name).st_size)
                workers.list[nodes[i]].client.send(file_size)
                time.sleep(1)
                file_size = int(file_size)
                segment_file = open(file_name, "rb")
                workers.list[nodes[i]].client.send(ext)

                # sets up progress bar
                print "[*] Sending segment-" + str(i) + ext, "to", workers.list[nodes[i]].name
                progress_interval = file_size / 50
                progress = file_size - progress_interval
                sys.stdout.write("[%s]" % (" " * 50))
                sys.stdout.flush()
                sys.stdout.write("\b" * (50+1))

                # sends segment
                while file_size > 0:
                    data_buffer = segment_file.read(1024)
                    workers.list[nodes[i]].client.send(data_buffer)
                    file_size -= len(data_buffer)
                    if file_size < progress:
                        progress -= progress_interval
                        sys.stdout.write("#")
                        sys.stdout.flush()
                sys.stdout.write("\n")
                print "[*] segment-" + str(i) + ext, "sent"
                
                # starts manager to receive transcoded files
                file_name = work_dir + "transcoded-" + str(i) + ".mkv"
                thread.start_new_thread(manager, (workers, i, nodes, file_name))

            # checks to see if all segments are complete
            print "[*] Waiting for file conversion to finish"
            while "no" in workers.segment_status: # and workers.audio_status != 0:
                time.sleep(30)

            # writes the concat file
            file_name = "instruct.txt"
            fp = open(file_name, "w+")
            for i in xrange(0, connected):
                data_buffer = "file \'" + work_dir + "transcoded-" + str(i) + ".mkv" + "\'\n"
                fp.write(data_buffer)
            fp.close()
            time.sleep(5)

            # concatenates all segments using ffmpeg
            # fields[0] is the orig filename without the extension
            print "[*] preparing to concatenate segments"
            command = "ffmpeg -f concat -i instruct.txt -c copy \"" + work_dir + fields[0] + ".mkv\""
            process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
            process.wait()
            workers.audio_status = 0
            workers.systemstatus = 0

            # adds audio to video
            command = "ffmpeg -i \"" + work_dir + fields[0] + ".mkv" + "\" -i \"" + load_dir + fname + "\" -map 0:0 -map 1:a -c:v copy -c:a copy \"" + targ_dir + fields[0] + ".mkv\""
            print "[*] Adding audio"
            process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
            process.wait()

            # removes file from load directory
            os.remove(file)

            # removes files from work directory
            # will remove all files in work directory
            file_list = os.listdir(work_dir)
            for file_name in file_list:
                if os.path.isfile(file_name):
                    os.remove(work_dir + "/" + file_name)

            # clears worker.segment_status
            del workers.segment_status[:]
            print "[*] Conversion Complete!"
        else:
            time.sleep(30)

if __name__ == '__main__':
    main()
